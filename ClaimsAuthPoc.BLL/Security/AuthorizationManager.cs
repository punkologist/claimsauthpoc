﻿using System;
using System.Linq;
using System.Security.Claims;

namespace ClaimsAuthPoc.BLL.Security
{
    public class AuthorizationManager : ClaimsAuthorizationManager
    {
        public override bool CheckAccess(AuthorizationContext context)
        {
            var resource = context.Resource[0].Value;
            var action = context.Action[0].Value;
            var principal = context.Principal;

            var role = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var applications = principal.Claims.Where(c => c.Type == CustomClaimTypes.Application);

            if (string.IsNullOrEmpty(role.Value) || !applications.Any())
            {
                return false;
            }

            //If the user has access to this application
            if (applications.Select(a => a.Value).Contains("ClaimsAuthPoc"))
            {
                //user has clicked on "about"
                if (resource == "Home" && action == "About")
                {
                    //user is user
                    if (role.Value == "IdentityServerSuperUsers")
                    {
                        return true;
                    }

                    return false;
                }
                return true;
            }
            return false;


        }
    }
}
