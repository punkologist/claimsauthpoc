﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ClaimsAuthPoc.Filters
{
    public class ClaimsAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string _resource;
        private readonly string _action;

        public ClaimsAuthorizeAttribute()
        {

        }
        public ClaimsAuthorizeAttribute(string resource, string action)
        {
            _resource = resource;
            _action = action;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return CheckAccess(httpContext);
        }

        protected virtual bool CheckAccess(HttpContextBase httpContext)
        {
            var resource = _resource ?? httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            var action = _action ?? httpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            return ClaimsAuthorizationWrapper.CheckAccess(
                resource,
                action);
        }

        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                // User is authenticated but doesn't have sufficent permissions. Redirect to InsufficientPermissions page
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                {
                    {"Controller", "Security"},
                    {"Action", "UnAuthorized"}
                });
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }

    }
}