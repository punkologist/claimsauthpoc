﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace ClaimsAuthPoc.Filters
{
    internal static class ClaimsAuthorizationWrapper
    {
        public static ClaimsAuthorizationManager AuthorizationManager
        {
            get { return FederatedAuthentication.FederationConfiguration.IdentityConfiguration.ClaimsAuthorizationManager; }
        }

        public static bool CheckAccess(string resource, string action)
        {
            var context = CreateAuthorisationContext(ClaimsPrincipal.Current, resource, action);
            return CheckAccess(context);
        }

        public static bool CheckAccess(AuthorizationContext context)
        {
            return AuthorizationManager.CheckAccess(context);
        }

        public static AuthorizationContext CreateAuthorisationContext(ClaimsPrincipal principal, string resource, string action)
        {

            return new AuthorizationContext(principal, resource, action);
        }

    }
}