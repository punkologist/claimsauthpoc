﻿using System;
using System.IdentityModel.Services;
using System.IdentityModel.Services.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace ClaimsAuthPoc.Controllers
{
    public class SecurityController : Controller
    {
        //
        // GET: /Security/
        public ActionResult Index()
        {
            return View("SignOut");
        }

        public void SignOut()
        {
            WsFederationConfiguration fc =
                   FederatedAuthentication.FederationConfiguration.WsFederationConfiguration;

            string request = System.Web.HttpContext.Current.Request.Url.ToString();
            string wreply = request.Substring(0, request.Length - 7);

            SignOutRequestMessage soMessage =
                            new SignOutRequestMessage(new Uri(fc.Issuer), wreply);
            soMessage.SetParameter("wtrealm", fc.Realm);

            FederatedAuthentication.SessionAuthenticationModule.SignOut();
            Response.Redirect(soMessage.WriteQueryString());
        }

        public ActionResult UnAuthorized()
        {

            return View();
        }
	}
}